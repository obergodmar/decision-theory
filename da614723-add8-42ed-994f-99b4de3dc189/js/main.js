"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var floor = Math.floor, exp = Math.exp;
var K = 167;
var data = [
    { a: 24, b: 10.2, c: 0.026, d: 0.25, p: 346, stages: 2 },
    { a: 23, b: 10.8, c: 0.013, d: 0.3, p: 792, stages: 2 },
    { a: 17, b: 15.0, c: 0.086, d: 0.25, p: 210, stages: 2 },
    { a: 24, b: 14.9, c: 0.039, d: 0.25, p: 692, stages: 3 }
];
/* Зависимость от количества внесенных удобрений */
var Fx = function (x, a, b, c) { return (a - (b * exp(-c * x))); };
/* Функция остатка за период подкормки */
var f = function (x, d) { return x * d; };
/*  Функция, находящая наибольший выигрыш */
var w = function (state, x, a, b, c) { return (Fx(state.rest + x, a, b, c)); };
/* Функция изменения состояния системы под влиянием управления */
var phi = function (state, x, d) {
    var rest = Number((f(state.rest + x, d)).toFixed(1));
    var remaining = Number((state.remaining - x).toFixed(1));
    return { rest: rest, remaining: remaining };
};
function main() {
    /* Необходимо посчитать количество удобрений на долю каждой культуры */
    data = data.map(function (rest) { return (__assign(__assign({}, rest), { pi: floor(rest.p * K
            / data.reduce(function (sum, next) { return sum += next.p; }, 0)) })); });
    data.forEach(function (_a, index) {
        var a = _a.a, b = _a.b, c = _a.c, d = _a.d, pi = _a.pi, stages = _a.stages;
        console.log("\n\u041A\u0443\u043B\u044C\u0442\u0443\u0440\u0430 K-" + (index + 1) + " (" + stages + " \u0441\u0442\u0430\u0434\u0438\u0438) \u0420\u0435\u0441\u0443\u0440\u0441: " + pi + "\n");
        /* Создаем массив из Map, в каждом из которых ключом является состояние системы */
        var map = Array(stages + 1).fill(new Map());
        /* Изменение состояния производится с конечной стадии */
        for (var stage = stages - 1; stage >= 0; --stage) {
            for (var remaining = 0; remaining < pi + 1; ++remaining) {
                /* Контролируем количество используемых удобрений */
                var maxRest = f(pi - remaining, d);
                for (var rest = 0; rest < maxRest + 1; ++rest) {
                    /* Инициализируем начальное состояние */
                    var state = { rest: rest, remaining: remaining };
                    var bestWin = -1;
                    var result = 0; // Будущий результат
                    for (var res = 0; res < remaining + 1; ++res) {
                        /* Изменяем состояние под влиянием управления */
                        var newState = phi(state, res, d);
                        /* Используется следующая стадия */
                        var mapResult = map[stage + 1].get(JSON.stringify(newState)) || 0;
                        /* Просчет выигрыша */
                        var win = w(state, res, a, b, c) + mapResult;
                        if (win > bestWin) {
                            bestWin = win;
                            result = res;
                        }
                    }
                    map[stage].set(JSON.stringify(state), result);
                }
            }
        }
        var resource = pi;
        var used = 0;
        var left = f(used, d);
        for (var stage = 0; stage < stages; ++stage) {
            var mapResult = map[stage].get(JSON.stringify({ rest: left, remaining: resource - used })) || resource - used;
            console.log("\u0421\u0442\u0430\u0434\u0438\u044F " + (stage + 1) + "\n                \u041E\u0441\u0442\u0430\u0442\u043E\u043A \u0443\u0434\u043E\u0431\u0440\u0435\u043D\u0438\u0439: " + (resource - used) + "\n                \u041D\u0430\u0438\u043B\u0443\u0447\u0448\u0438\u0439 \u0432\u0430\u0440\u0438\u0430\u043D\u0442: " + mapResult);
            resource -= used;
            used = mapResult;
            left = f(used, d);
        }
    });
}
main();
