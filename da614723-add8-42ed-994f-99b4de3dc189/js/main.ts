const { floor, exp } = Math

const K = 167
let data: Data[] = [
    { a: 24, b: 10.2,   c: 0.026, d: 0.25,  p: 346, stages: 2 },
    { a: 23, b: 10.8,   c: 0.013, d: 0.3,   p: 792, stages: 2 },
    { a: 17, b: 15.0,   c: 0.086, d: 0.25,  p: 210, stages: 2 },
    { a: 24, b: 14.9,   c: 0.039, d: 0.25,  p: 692, stages: 3 }
]

/* Тип исходных данных */
type Data = {
    a: number
    b: number
    c: number
    d: number
    p: number
    pi?: number
    stages: number
}

/* Тип состояния, включающее в себя оставшееся количество удобрений (rest)
   и остаток удобрений после последнего внесения (remaining) */
interface State {
    remaining: number
    rest: number
}

/* Зависимость от количества внесенных удобрений */
const Fx = (
    x: number,
    a: number,
    b: number,
    c: number
): number => (
    a - (b * exp(-c * x))
)

/* Функция остатка за период подкормки */
const f = (x: number, d: number): number => x * d

/*  Функция, находящая наибольший выигрыш */
const w = (
    state: State,
    x: number,
    a: number,
    b: number,
    c: number
): number => (
    Fx(state.rest + x, a, b, c)
)

/* Функция изменения состояния системы под влиянием управления */
const phi = (state: State, x: number, d: number): State => {
    const rest = Number((f(state.rest + x, d)).toFixed(1))
    const remaining = Number((state.remaining - x).toFixed(1))
    return {rest, remaining}
}

function main() {
    /* Необходимо посчитать количество удобрений на долю каждой культуры */
    data = data.map(rest => ({
        ...rest,
        pi: floor(
            rest.p * K
            / data.reduce((sum, next) => sum += next.p, 0)
        )
    }))

    data.forEach(({a, b, c, d, pi, stages}, index) => {
        console.log(`\nКультура K-${index+1} (${stages} стадии) Ресурс: ${pi}\n`)
        /* Создаем массив из Map, в каждом из которых ключом является состояние системы */
        let map: Map<string, number>[] = Array(stages + 1).fill(new Map<string, number>())
        /* Изменение состояния производится с конечной стадии */
        for (let stage = stages - 1; stage >= 0; --stage) {
            for (let remaining = 0; remaining < pi! + 1; ++remaining) {
                /* Контролируем количество используемых удобрений */
                let maxRest = f(pi! - remaining, d)
                for (let rest = 0; rest < maxRest + 1; ++rest) {
                    /* Инициализируем начальное состояние */
                    let state: State = { rest, remaining }
                    let bestWin = -1
                    let result = 0 // Будущий результат
                    for (let res = 0; res < remaining + 1; ++res) {
                        /* Изменяем состояние под влиянием управления */
                        let newState: State = phi(state, res, d)
                        /* Используется следующая стадия */
                        let mapResult = map[stage+1].get(JSON.stringify(newState)) || 0
                        /* Просчет выигрыша */
                        let win = w(state, res, a, b, c) + mapResult
                        if (win > bestWin) {
                            bestWin = win
                            result = res
                        }
                    }
                    map[stage].set(JSON.stringify(state), result)
                }
            }
        }

        let resource = pi!
        let used = 0
        let left = f(used, d)

        for (let stage = 0; stage < stages; ++stage) {
            let mapResult = map[stage].get(JSON.stringify({rest: left, remaining: resource - used})) || resource - used
            console.log(`Стадия ${stage + 1}
                Остаток удобрений: ${resource - used}
                Наилучший вариант: ${mapResult}`)
            resource -= used
            used = mapResult
            left = f(used, d)
        }
    })
}

main()
